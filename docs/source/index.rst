.. openvpn-server documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to openvpn-server's documentation!
==========================================

.. toctree::
   :caption: Contents:

   OpenVPNServer <openvpn_server>
   OpenVPNConfig <openvpn_config>
   exceptions

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
