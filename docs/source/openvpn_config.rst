OpenVPNConfig
=====================

.. autoclass:: openvpn_server.OpenVPNConfig
    :members:
    :special-members: __init__
