import os
import time
from pathlib import Path

import pytest

import openvpn_server as ovpnd

CI = bool(os.environ.get("CI", False))


def create_test_config():
    test_config = {
        "dev": "tap",
        "port": "5640",
        "proto": "udp",
        "server-bridge": "10.8.0.1 255.255.255.0 10.8.0.100 10.8.0.254",
        "keepalive": "10 120",
        "comp-lzo": "",
        "explicit-exit-notify": "",
    }
    keys_dir = Path("tests") / "conf" / "keys"
    with keys_dir / "ca.crt" as ca, keys_dir / "server.crt" as cert, keys_dir / "server.key" as key, keys_dir / "dh2048.pem" as dh:
        tmp = ovpnd.OpenVPNConfig(
            ca=ca.read_text(), cert=cert.read_text(), key=key.read_text(), dh=dh.read_text(), **test_config
        )
    return tmp


@pytest.mark.sudo
def test_start_server():
    config = create_test_config()
    config.config["port"] = "5641"
    with ovpnd.OpenVPNServer(config=config) as server:
        assert server.is_running
        time.sleep(1)
        assert server.is_running


@pytest.mark.sudo
def test_double_stop():
    config = create_test_config()
    config.config["port"] = "5642"
    with ovpnd.OpenVPNServer(config=config) as server:
        assert server.is_running
        server.stop()
        assert not server.is_running
        server.stop()
        assert not server.is_running


@pytest.mark.sudo
def test_is_running():
    config = create_test_config()
    config.config["port"] = "5643"
    with ovpnd.OpenVPNServer(config=config) as server:
        assert server.is_running
        server.stop()
        assert not server.is_running


@pytest.mark.sudo
def test_readonly_config():
    config = create_test_config()
    with ovpnd.OpenVPNServer(config=config) as server:
        running_config = server.get_config()
        assert config is not running_config
        assert config.config is not running_config.config


@pytest.mark.sudo
def test_basic_states():
    config = create_test_config()
    config.config["port"] = "5644"
    with ovpnd.OpenVPNServer(config=config) as server:
        assert server.state == "CONNECTED"
        server.stop()
        assert server.state == "NOT_RUNNING"


@pytest.mark.skipif(not CI, reason="this test leaks ovpn instances")
@pytest.mark.sudo
def test_startup_timeout():
    config = create_test_config()
    config.config["port"] = "5645"
    with pytest.raises(
        ovpnd.OVPNStartupTimeoutException,
        match=r"OVPN with PID [0-9]+ was not ready after specified timeout \([0-9]+s\)",
    ):
        ovpnd.OpenVPNServer(config=config, startup_timeout=0)


@pytest.mark.sudo
def test_stop_timeout():
    config = create_test_config()
    config.config["port"] = "5646"
    server = ovpnd.OpenVPNServer(config=config)
    with pytest.raises(
        ovpnd.OVPNStopTimeoutException, match=r"OVPN was still alive after specified timeout \([0-9]+s\)",
    ):
        server.stop(timeout=0)
