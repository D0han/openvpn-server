from pathlib import Path

import openvpn_server as ovpnd


def test_config_generation():
    test_config = {
        "dev": "tap",
        "port": "5647",
        "proto": "udp",
        "server-bridge": "10.8.0.1 255.255.255.0 10.8.0.100 10.8.0.254",
        "keepalive": "10 120",
        "comp-lzo": "",
        "explicit-exit-notify": "",
    }
    keys_dir = Path("tests") / "conf" / "keys"
    with keys_dir / "ca.crt" as ca, keys_dir / "server.crt" as cert, keys_dir / "server.key" as key, keys_dir / "dh2048.pem" as dh:
        config_instance = ovpnd.OpenVPNConfig(
            ca=ca.read_text(), cert=cert.read_text(), key=key.read_text(), dh=dh.read_text(), **test_config
        )
    expected_config = Path("tests") / "conf" / "server.conf"
    assert config_instance.get_content() == expected_config.read_bytes()
